package stupidthings;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import stupidthings.entities.ExplodingPigEntity;
import stupidthings.items.PigLauncher;

public class StupidThings implements ModInitializer {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String MOD_ID = "stupidthings";
    private static final String MOD_NAME = "StupidThings";

    // Add the amazing exploding pig launcher
    private static final PigLauncher PIG_LAUNCHER = new PigLauncher(new Item.Settings().group(ItemGroup.MISC).maxDamage(100));

    // Add exploding pig
    public static final EntityType<ExplodingPigEntity> EXPLODING_PIG =
            Registry.register(
                    Registry.ENTITY_TYPE,
                    new Identifier(StupidThings.MOD_ID, "exploding-pig"),
                    FabricEntityTypeBuilder.create(EntityCategory.AMBIENT, ExplodingPigEntity::new).size(EntityDimensions.fixed(1, 2)).build()
            );


    @Override
    public void onInitialize() {
        log("Initializing");
        Registry.register(Registry.ITEM, new Identifier(StupidThings.MOD_ID, "pig_launcher"), PIG_LAUNCHER);
        log("Initialized Have fun!");
    }

    private static void log(String message){
        LOGGER.log(Level.INFO, "["+MOD_NAME+"] " + message);
    }

}
