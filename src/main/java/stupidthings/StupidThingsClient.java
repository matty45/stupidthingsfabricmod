package stupidthings;


import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.PigEntityRenderer;

public class StupidThingsClient implements ClientModInitializer {


    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.INSTANCE.register(StupidThings.EXPLODING_PIG, (entityRenderDispatcher, context) -> new PigEntityRenderer(entityRenderDispatcher));
        // System.out.println("Client Initialized");
    }

}