package stupidthings.items;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import stupidthings.StupidThings;

public class PigLauncher extends Item
{
    public PigLauncher(Settings settings)
    {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity Player, Hand hand)
    {
        ItemStack ItemInHand = Player.getStackInHand(hand);
        int damage = ItemInHand.getDamage();
        World mainworld = Player.getEntityWorld();
        if (damage != ItemInHand.getMaxDamage() & !mainworld.isClient) {
            stupidthings.entities.ExplodingPigEntity piggy = new stupidthings.entities.ExplodingPigEntity(StupidThings.EXPLODING_PIG,mainworld);
            Vec3d playerPos = Player.getPos();
            piggy.updatePosition(playerPos.x, Player.getEyeY() - 0.15000000596046448D, playerPos.z);
            piggy.setHealth(1);
            mainworld.spawnEntity(piggy);
            Vec3d rotationVector = Player.getRotationVector();
            Vec3d aimvector = new Vec3d(rotationVector.x * 10, rotationVector.y * 10, rotationVector.z * 10);
            piggy.setVelocity(aimvector);
            Player.getMainHandStack().damage(10, world.random, null);
        }
        return new TypedActionResult<>(ActionResult.SUCCESS, ItemInHand);
    }
}